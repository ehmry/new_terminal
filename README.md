# New_Terminal

A mimimalist bitmapped rendering of [Wim Crouwel](https://en.wikipedia.org/wiki/Wim_Crouwel)'s
[New Alphabet](https://en.wikipedia.org/wiki/New_Alphabet) typeface.

![specimen](specimen.png)
