{
  description = "development flake";
  outputs = { self, nixpkgs }: {
    devShell.x86_64-linux = with nixpkgs.legacyPackages.x86_64-linux; mkShell {
      packages = [ fontforge bdf2sfd imagemagick tup xorg.bdftopcf ];
    };
  };
}
